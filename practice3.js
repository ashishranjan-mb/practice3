const users = [{
    firstName: "Emily",
    lastName: "Blunt",
    age: 23
}, {
    firstName: "Carlos",
    lastName: "Vela",
    age: 23
}, {
    firstName: "Marcus",
    lastName: "Arnold",
    age: 21
}, {

    firstName: "Leo",
    lastName: "Wesley",
    age: 24

}]


/*

    Q. Whats the average age of the users.
    Q. Transform users array to give just firstName and lastName for each user.
    Q. Get all fullNames of each user sorted in alphabetical order.
        LastName comes before FirstName.
    Q. Transform users array to 
    [
        ["Emily", "Blunt", 23],
        ["Vela", "Carlos", 23], 
        ["Marcus", "Arnold", 21], 
        ["Wesley", "Leo", 24 ]
    ]

    Q. Transform users array to 
        [
            "Emily | Blunt, age: 23",
            "Carlos | Vela, age: 23", 
            "Marcus | Arnold, age: 21", 
            "Wesley | Leo, age: 24"
        ]

*/

//PROBLEM 1
const averageAge = (arr) => {
    const {length} = arr;
    return arr.reduce((acc, val) => {
       return acc + (val.age/length);
    },0);
 };
 console.log(averageAge(users));

//PROBLEM 2
let getAllName = users.map(item => ({ [item.firstName]: item.lastName }) );
let res = Object.assign({}, ...getAllName);
console.log(res);

//PROBLEM 3
const getFullName = users.sort((a, b) => {
    if (a.lastName < b.lastName)
        return -1;
})
console.log(getFullName);

//PROBLEM 4
let res1 = users.map(obj => Object.values(obj));
console.log(res1)

//PROBLEM 5
function convertdata(users){
    return users.reduce((acc,curr) => {
        const data = `${curr.firstName} | ${curr.lastName}, age: ${curr.age}`;
        acc.push(data);
        return acc;
    }, [])
}
console.log(convertdata(users));
